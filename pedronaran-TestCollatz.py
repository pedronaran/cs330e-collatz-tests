#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, collatz_compute

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
    
    def test_read_2(self):
        s = "56 7"
        i, j = collatz_read(s)
        self.assertEqual(i,  56)
        self.assertEqual(j, 7)
    
    def test_read_3(self):
        s = "\n4 67"
        i, j = collatz_read(s)
        self.assertEqual(i,  4)
        self.assertEqual(j, 67)
    

    # ----
    # compute
    # ----

    def test_compute_1(self):
        v = collatz_compute(1, 10)
        self.assertEqual(v, 20)

    def test_compute_2(self):
        v = collatz_compute(100, 200)
        self.assertEqual(v, 125)



    # ----
    # eval
    # ----

    def test_eval_1(self):
        v, cache = collatz_eval(1, 10, {(1, 90000): 351})
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v, cache= collatz_eval(100, 200, {(100, 150): 122, (100, 101): 26})
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v, cache= collatz_eval(201, 210, {(201, 210): 89})
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v, cache= collatz_eval(900, 1000, {(950, 1000): 143})
        self.assertEqual(v, 174)
    
    def test_eval_5(self):
        v, cache= collatz_eval(377, 2, dict())
        self.assertEqual(v, 144)
    
    def test_eval_6(self):
        v, cache= collatz_eval(1, 90000, dict())
        self.assertEqual(v, 351)
    
    def test_eval_7(self):
        v, cache= collatz_eval(88, 88, dict())
        self.assertEqual(v, 18)
    
    def test_eval_8(self):
        v, cache= collatz_eval(9897, 9880, dict())
        self.assertEqual(v, 242)
    
    def test_eval_9(self):
        v, cache= collatz_eval(900, 1000, {(950, 980): 143})
        self.assertEqual(v, 174)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 210, 201, 89)
        self.assertEqual(w.getvalue(), "210 201 89\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 88, 88, 18)
        self.assertEqual(w.getvalue(), "88 88 18\n")
    
    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 1, 90000, 351)
        self.assertEqual(w.getvalue(), "1 90000 351\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
    
    def test_solve_2(self):
        r = StringIO("87 120\n155 299\n2 34\n43424 2424\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "87 120 119\n155 299 128\n2 34 112\n43424 2424 324\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
